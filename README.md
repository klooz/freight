# freight

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# Run with Docker 

## Build Image
```
docker build -t registry.gitlab.com/klooz/freight .
```

## Push Image
```
docker push registry.gitlab.com/klooz/freight
```

## Pull Image
```
docker pull registry.gitlab.com/klooz/freight
```

## Run Image
```
docker run -p 80:80 --rm --name freight registry.gitlab.com/klooz/freight
```