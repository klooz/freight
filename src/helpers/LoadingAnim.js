import Vue from 'vue'
import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'

Vue.use(Loading, {
  color: '#0071BB',
  loader: 'bars',
  height: 100,
  width: 100
})

export default {}
