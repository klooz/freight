import Vue from 'vue'
import Vuex from 'vuex'
import MainService from '@/services/MainService.js'
import '@/helpers/LoadingAnim.js'
import moment from 'moment'

Vue.use(Vuex)

const state = {
  onLoading: false,
  loadStack: 0,
  loadingAnim: Vue.$loading,
  userdata: {
    name: '',
    avatar: ''
  },
  menu: [],
  dashboard: {},
  transport: [],
  transportType: [],
  fromCountry: [],
  fromCountryLocation: [],
  toCountry: [],
  toCountryLocation: [],
  shipment: [],
  container: [],
  tableResultHeaders: [],
  tableResultItems: [],
  dataSearch: {},
  dataResult: [],
  dataResultHeader: [],
  dataResultDetail: [],
  histories: [],
  historyDataSearch: {
    agent: [],
    transport: [],
    transportType: [],
    country: []
  },
  resultHeaderHistory: [],
  exchangeRate: [
    {
      currencyCode: 'USD',
      rate: 30.0
    },
    {
      currencyCode: 'EUR',
      rate: 35.0
    },
    {
      currencyCode: 'JPY',
      rate: 0.3
    }
  ],
  dataForPrint: [],
  idListByAgent: [],
  invoice: {},
  memberError: '',
  members: [],
  departments: [],
  allDepartments: [],
  userTypeList: [],
  agentError: '',
  agents: [],
  expenses: [],
  expenseName: [],
  expenseDetail: [],
  expenseDateDetail: [],
  requests: [],
  importList: [],
  importDetail: [],
  importPrice: [],
  importUpload: [],
  statusList: [],
  cbmError: '',
  cbms: [],
  vatError: '',
  vats: [],
  currencies: [],
  foreignExchangeRateError: '',
  foreignExchangeRate: [],
  expenseNameError: '',
  expenseDetailError: '',
  expenseAgentError: '',
  requestError: '',
  requestApproveError: '',
  requestCancelError: '',
  importDetailError: '',
  importPriceError: '',
  importUploadError: ''
}
const mutations = {
  LOADING(state) {
    if (!state.loadingAnim) state.loadingAnim = Vue.$loading

    state.loadStack++
    if (!state.onLoading && state.loadStack <= 1) {
      state.onLoading = true
      state.loadingAnim = state.loadingAnim.show()
    }
  },
  LOAD_DONE(state) {
    if (state.onLoading) {
      state.loadStack = state.loadStack - 1
      if (state.loadStack == 0) {
        state.onLoading = false
        state.loadingAnim = state.loadingAnim.hide()
      }
    }
  },
  UPDATE_USERDATA(state, newValue) {
    state.userdata = newValue
  },
  UPDATE_TABLE_RESULT_HEADERS(state, data) {
    if (data.type.toLowerCase() == 'sea') {
      state.tableResultHeaders = [
        { text: 'Agent/Weight', value: 'user_agent' },
        // { text: 'Create Date', value: 'createDate' },
        { text: 'Departure', value: 'from' },
        { text: 'Type', value: 'type' },
        { text: 'CUT OFF', value: 'cutoff' },
        { text: 'ETD', value: 'etd' },
        { text: 'Period', value: 'eta' },
        { text: 'Arrive', value: 'to' },
        { text: 'Direct / Transit', value: 'tranship' },
        { text: 'Cost/Unit', value: 'unit' },
        { text: 'Total Cost', value: 'amount' },
        { text: 'Management', value: 'manage', sortable: false }
      ]
    } else {
      state.tableResultHeaders = [
        { text: 'Agent/Weight', value: 'user_agent' },
        // { text: 'Create Date', value: 'createDate' },
        { text: 'Day / Start - End Date', value: 'date' },
        { text: 'Departure', value: 'from' },
        { text: 'Arrive', value: 'to' },
        { text: 'Carrier', value: 'carrer' },
        { text: 'Direct / Transit', value: 'tranship' },
        { text: 'Cost/Unit', value: 'unit' },
        { text: 'Total Cost', value: 'amount' },
        { text: 'Management', value: 'manage', sortable: false }
      ]
    }
  },
  UPDATE_TABLE_RESULT_ITEMS(state, newValue) {
    state.tableResultItems = newValue
  },
  UPDATE_RESULT_HEADER_HISTORY(state, newValue) {
    state.resultHeaderHistory = newValue.dataSet
    state.exchangeRate = newValue.dataSet2
  },
  UPDATE_CALC_SEARCH(state, newValue) {
    state.dataSearch = newValue
  },
  UPDATE_INVOICE(state, newValue) {
    state.invoice = newValue
  },
  UPDATE_DATA_RESULT_HEADER(state, newValue) {
    state.dataResultHeader = newValue
  },
  UPDATE_MEMBER(state, newValue) {
    state.members = newValue
  },
  UPDATE_DEPARTMENT(state, newValue) {
    state.departments = newValue
  },
  UPDATE_ALL_DEPARTMENT(state, newValue) {
    state.allDepartments = newValue
  },
  UPDATE_USER_TYPE(state, newValue) {
    state.userTypeList = newValue
  },
  UPDATE_AGENT(state, newValue) {
    state.agents = newValue
  },
  UPDATE_REQUEST(state, newValue) {
    state.requests = newValue
  },
  UPDATE_IMPORT_UPLOAD(state, newValue) {
    state.importUpload = newValue
  },
  UPDATE_STATUS(state, newValue) {
    state.statusList = newValue
  },
  UPDATE_IMPORT(state, newValue) {
    state.importList = newValue
  },
  UPDATE_IMPORT_DETAIL(state, newValue) {
    state.importDetail = newValue
  },
  UPDATE_IMPORT_PRICE(state, newValue) {
    state.importPrice = newValue
  },
  UPDATE_EXPENSE(state, newValue) {
    state.expenses = newValue
  },
  UPDATE_EXPENSE_NAME(state, newValue) {
    state.expenseName = newValue
  },
  UPDATE_EXPENSE_DETAIL(state, newValue) {
    state.expenseDetail = newValue
  },
  UPDATE_EXPENSE_DATE_DETAIL(state, newValue) {
    state.expenseDateDetail = newValue
  },
  UPDATE_VAT(state, newValue) {
    state.vats = newValue
  },
  UPDATE_CBM(state, newValue) {
    state.cbms = newValue
  },
  UPDATE_CURRENCY(state, newValue) {
    state.currencies = newValue
  },
  UPDATE_FOREIGN_EXCHANGE_RATE(state, newValue) {
    state.foreignExchangeRate = newValue
  },
  COLLECT_PRINTING_DATA(state, newValue) {
    state.dataForPrint.push(newValue)
  },
  COLLECT_ID_BY_AGENT(state, newValue) {
    state.idListByAgent.push(...newValue)
  },
  CLEAR_PRINTING_DATA(state) {
    state.dataForPrint = []
  },
  CLEAR_TABLE_RESULT(state) {
    state.tableResultHeaders = []
    state.tableResultItems = []
    state.histories = []
  },
  CLEAR_ID_BY_AGENT(state) {
    state.idListByAgent = []
  },
  ERROR_MEMBER(state, newValue) {
    state.memberError = newValue
  },
  ERROR_AGENT(state, newValue) {
    state.agentError = newValue
  },
  ERROR_CBM(state, newValue) {
    state.cbmError = newValue
  },
  ERROR_VAT(state, newValue) {
    state.vatError = newValue
  },
  ERROR_FOREIGN_EXCHANGE_RATE(state, newValue) {
    state.foreignExchangeRateError = newValue
  },
  ERROR_EXPENSE_NAME(state, newValue) {
    state.expenseNameError = newValue
  },
  ERROR_EXPENSE_DETAIL(state, newValue) {
    state.expenseDetailError = newValue
  },
  ERROR_REQUEST(state, newValue) {
    state.requestError = newValue
  },
  ERROR_REQUEST_APPROVE(state, newValue) {
    state.requestApproveError = newValue
  },
  ERROR_REQUEST_CANCEL(state, newValue) {
    state.requestCancelError = newValue
  },
  ERROR_IMPORT_UPLOAD(state, newValue) {
    state.importUploadError = newValue
  },
  ERROR_EXPENSE_AGENT(state, newValue) {
    state.expenseAgentError = newValue
  },
  ERROR_IMPORT_DETAIL(state, newValue) {
    state.importDetailError = newValue
  },
  ERROR_IMPORT_PRICE(state, newValue) {
    state.importPriceError = newValue
  }
}
const actions = {
  accessToken({ commit, dispatch }, payload) {
    if (payload.u && payload.p) {
      commit('LOADING')
      return MainService.accessToken(payload.u, payload.p)
        .then(function(response) {
          if (!response.data.status || response.data.status === 'false') {
            console.log('LOGIN FALSE!!!')
            localStorage.clear()
          } else {
            localStorage.setItem('loginToken', response.data.dataSet.tokenId)
            localStorage.setItem('userTypeId', response.data.dataSet.userTypeId)
            localStorage.setItem('userType', response.data.dataSet.userType)
            localStorage.setItem('email', response.data.dataSet.email)
            localStorage.setItem('firstName', response.data.dataSet.firstName)
            localStorage.setItem('lastName', response.data.dataSet.lastName)
            localStorage.setItem('setupTime', new Date().getTime())
            commit('UPDATE_USERDATA', {
              name:
                response.data.dataSet.firstName +
                ' ' +
                response.data.dataSet.lastName,
              avatar:
                response.data.dataSet.firstName.substr(0, 1) +
                ' ' +
                response.data.dataSet.lastName.substr(0, 1)
            })
          }
        })
        .catch(function(error) {
          console.log(error)
        })
        .then(function() {
          dispatch('getMenu')
          commit('LOAD_DONE')
        })
    }
  },
  checkTokenExpired({ commit }) {
    commit('LOADING')
    return MainService.checkTokenExpired()
      .then(function(response) {
        localStorage.setItem('tokenActive', response.status)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getMenu({ state, commit }) {
    if (localStorage.getItem('loginToken') != null) {
      commit('LOADING')
      return MainService.getMenu()
        .then(function(response) {
          if (!response.data.status) {
            localStorage.clear()
            window.location = '/'
          }
          let data = response.data.dataSet
          let icon
          let route
          data = data.map(item => {
            let res = {
              title: item.menuDisplay,
              flag: item.menuFlag
            }
            switch (item.menuName) {
              case 'MemberManagement':
                icon = 'mail_outline'
                route = '/member'
                break
              case 'AgentManagement':
                icon = 'mdi-chart-bar'
                route = '/agent'
                break
              case 'ExpenseManagement':
                icon = 'mdi-diamond-stone'
                route = '/expense'
                break
              case 'RequestNewLocation':
                icon = 'calendar_today'
                route = '/request'
                break
              case 'ImportExportData':
                icon = 'calendar_today'
                route = '/import'
                break
              case 'Calculator':
                icon = 'mdi-chart-bar'
                route = '/calculator'
                break
              case 'History':
                icon = 'mdi-chart-bar'
                route = '/history'
                break
              case 'Setting':
                icon = 'mdi-flask-outline'
                route = '/setting'
                break
            }
            res.icon = icon
            if (item.subMenus.length == 0) {
              res.route = route
            } else {
              res.subroute = []
              res.subitems = item.subMenus.map(subItem => {
                let subRes = {
                  title: subItem.subMenuDisplay,
                  flag: subItem.subMenuFlag
                }
                switch (subItem.subMenuName) {
                  case 'CBM':
                    subRes.route = '/cbm'
                    break
                  case 'ExchangeRate':
                    subRes.route = '/exchange'
                    break
                  case 'Vat':
                    subRes.route = '/vat'
                    break
                }
                res.subroute.push(subRes.route)
                return subRes
              })
            }
            return res
          })
          state.menu = [
            {
              icon: 'home',
              title: 'Home',
              route: '/',
              flag: null
            },
            ...data
          ]
        })
        .catch(function(error) {
          console.log(error)
        })
        .then(function() {
          commit('LOAD_DONE')
        })
    }
  },
  getDashboard({ state, commit }) {
    commit('LOADING')
    return MainService.getDashboard()
      .then(function(response) {
        state.dashboard = response.data
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getTransport({ state, commit }) {
    commit('LOADING')
    return MainService.getTransport()
      .then(function(response) {
        state.transport = response.data.dataSet
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getTransportType({ state, commit }) {
    commit('LOADING')
    return MainService.getTransportType()
      .then(function(response) {
        state.transportType = [
          {
            transportTypeId: 0,
            transportTypeDesc: 'Choose...',
            port: 'Choose...'
          },
          ...response.data.dataSet
        ]
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getShipment({ state, commit }) {
    commit('LOADING')
    return MainService.getShipment()
      .then(function(response) {
        state.shipment = [
          {
            shipmentId: 0,
            shipmentDesc: 'Choose...'
          },
          ...response.data.dataSet
        ]
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getContainers({ state, commit }) {
    commit('LOADING')
    return MainService.getContainers()
      .then(function(response) {
        state.container = [
          {
            containerId: 0,
            containerSize: 'Choose...',
            containerWeight: 0,
            containerDesc: 'Choose...'
          },
          ...response.data.dataSet
        ]
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getCountry({ state, commit }, payload) {
    commit('LOADING')
    return Promise.all([
      MainService.getCountry(
        payload.transportId,
        payload.transportTypeId,
        'From'
      ),
      MainService.getCountry(payload.transportId, payload.transportTypeId, 'To')
    ])
      .then(([from, to]) => {
        state.fromCountry = from.data.dataSet
        state.fromCountryLocation = []
        state.toCountry = to.data.dataSet
        state.toCountryLocation = []
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getCountryLocation({ state, commit }, payload) {
    commit('LOADING')
    return MainService.getCountryLocation(
      payload.transportTypeId,
      payload.country
    )
      .then(function(response) {
        let sorted = response.data.dataSet.sort((a, b) => {
          return a.countryLocationId - b.countryLocationId
        })
        if (payload.type == 'From') {
          state.fromCountryLocation = sorted
        } else {
          state.toCountryLocation = sorted
        }
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getResultHeader({ commit }, payload) {
    commit('LOADING')
    return MainService.getResultHeader(payload)
      .then(function(response) {
        commit('UPDATE_DATA_RESULT_HEADER', response.data.dataSet[0])
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getResultDetail({ state, commit }, payload) {
    commit('LOADING')
    return MainService.getResultDetail(payload)
      .then(function(response) {
        state.dataResultDetail = response.data.dataSet
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getHistory({ state, commit }, payload) {
    commit('LOADING')
    return MainService.getHistory(payload)
      .then(function(response) {
        state.histories = response.data.dataSet
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getHistoryDataSearch({ state, commit }) {
    commit('LOADING')
    return MainService.getHistoryDataSearch()
      .then(function(response) {
        console.log(response)
        state.historyDataSearch.agent = response.data.agent
        state.historyDataSearch.transport = response.data.transport
        state.historyDataSearch.transportType = response.data.transportType
        state.historyDataSearch.country = response.data.country
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  async getResultHeaderHistory({ commit }, payload) {
    commit('LOADING')
    await MainService.getResultHeaderHistory(payload)
      .then(function(response) {
        commit('UPDATE_RESULT_HEADER_HISTORY', response.data)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  async getPrintingData({ commit }, payload) {
    commit('LOADING')
    commit('CLEAR_PRINTING_DATA')
    for (let item of payload) {
      await Promise.all([
        MainService.getResultHeader(item.resultHeaderId),
        MainService.getResultDetail(item.resultHeaderId)
      ])
        .then(([header, detail]) => {
          let data = {
            search: item,
            header: header.data.dataSet[0],
            detail: detail.data.dataSet
          }
          commit('COLLECT_PRINTING_DATA', data)
        })
        .catch(function(error) {
          console.log(error)
        })
        .then(function() {
          commit('LOAD_DONE')
        })
    }
  },
  async getResultHeaderIdByAgent({ commit }, payload) {
    commit('LOADING')
    commit('CLEAR_ID_BY_AGENT')
    for (let item of payload) {
      await MainService.getResultHeaderHistory(item.id)
        .then(function(response) {
          commit(
            'COLLECT_ID_BY_AGENT',
            response.data.dataSet.map(resItem => {
              return { ...item, resultHeaderId: resItem.resultHeaderId }
            })
          )
        })
        .catch(function(error) {
          console.log(error)
        })
        .then(function() {
          commit('LOAD_DONE')
        })
    }
  },
  getInvoice({ commit }, payload) {
    commit('LOADING')
    return MainService.getInvoice(payload)
      .then(function(response) {
        commit('UPDATE_INVOICE', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getMember({ commit }, payload) {
    commit('LOADING')
    return MainService.getMember(payload)
      .then(function(response) {
        commit('UPDATE_MEMBER', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getDepartment({ commit }) {
    commit('LOADING')
    return MainService.getDepartment()
      .then(function(response) {
        console.log(response)
        commit('UPDATE_DEPARTMENT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getAllDepartment({ commit }) {
    commit('LOADING')
    return MainService.getAllDepartment()
      .then(function(response) {
        commit('UPDATE_ALL_DEPARTMENT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getUserType({ commit }) {
    commit('LOADING')
    return MainService.getUserType()
      .then(function(response) {
        commit('UPDATE_USER_TYPE', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getAgent({ commit }, payload) {
    commit('LOADING')
    return MainService.getAgent(payload)
      .then(function(response) {
        commit('UPDATE_AGENT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getExpense({ commit }, payload) {
    commit('LOADING')
    return MainService.getExpense(payload)
      .then(function(response) {
        commit('UPDATE_EXPENSE', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getExpenseName({ commit }) {
    commit('LOADING')
    return MainService.getExpenseName()
      .then(function(response) {
        commit('UPDATE_EXPENSE_NAME', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getExpenseDetail({ commit }, payload) {
    commit('LOADING')
    return MainService.getExpenseDetail(payload)
      .then(function(response) {
        commit('UPDATE_EXPENSE_DETAIL', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getExpenseDateDetail({ commit }, payload) {
    commit('LOADING')
    return MainService.getExpenseDateDetail(payload)
      .then(function(response) {
        commit('UPDATE_EXPENSE_DATE_DETAIL', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getRequest({ commit }, payload) {
    commit('LOADING')
    return MainService.getRequest(payload)
      .then(function(response) {
        commit('UPDATE_REQUEST', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getRequestApprove({ commit }, payload) {
    commit('LOADING')
    return MainService.getRequestApprove(payload)
      .then(function(response) {
        if (response.data.status == false || response.data.status == 'false') {
          commit('ERROR_REQUEST_APPROVE', response.data.message)
        }
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getRequestCancel({ commit }, payload) {
    commit('LOADING')
    return MainService.getRequestCancel(payload)
      .then(function(response) {
        if (response.data.status == false || response.data.status == 'false') {
          commit('ERROR_REQUEST_CANCEL', response.data.message)
        }
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getStatus({ commit }) {
    commit('LOADING')
    return MainService.getStatus()
      .then(function(response) {
        commit('UPDATE_STATUS', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getImportList({ commit }, payload) {
    commit('LOADING')
    let transportId = 0
    let transportTypeId = 0
    let statusId = 0
    if (payload) {
      transportId = payload.transportId
      transportTypeId = payload.transportTypeId
      statusId = payload.statusId
    }
    return MainService.getImportList(transportId, transportTypeId, statusId)
      .then(function(response) {
        commit('UPDATE_IMPORT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getImportDetail({ commit }, payload) {
    commit('LOADING')
    return MainService.getImportDetail(payload.id, payload.transportTypeId)
      .then(function(response) {
        commit('UPDATE_IMPORT_DETAIL', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getImportPrice({ commit }, payload) {
    commit('LOADING')
    return MainService.getImportPrice(payload.detailId, payload.transportTypeId)
      .then(function(response) {
        commit('UPDATE_IMPORT_PRICE', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getImportDownload({ commit }, payload) {
    commit('LOADING')
    return MainService.getImportDownload(
      payload.transportTypeId,
      payload.tableId
    )
      .then(function(response) {
        var fileURL = window.URL.createObjectURL(
          new Blob([response.data], { type: 'application/pdf;base64' })
        )
        var fileLink = document.createElement('a')
        fileLink.href = fileURL
        fileLink.setAttribute('download', payload.filename + '.xlsx')
        document.body.appendChild(fileLink)
        fileLink.click()
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getImportApprove({ commit }, payload) {
    commit('LOADING')
    return MainService.getImportApprove(payload.transportTypeId, payload.id)
      .then(function(response) {
        commit('UPDATE_IMPORT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getImportExpire({ commit }, payload) {
    commit('LOADING')
    return MainService.getImportExpire(payload.transportTypeId, payload.id)
      .then(function(response) {
        commit('UPDATE_IMPORT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getExpenseDownload({ commit }, payload) {
    commit('LOADING')
    return MainService.getExpenseDownload(payload.id)
      .then(function(response) {
        var fileURL = window.URL.createObjectURL(
          new Blob([response.data], { type: 'application/pdf;base64' })
        )
        var fileLink = document.createElement('a')
        fileLink.href = fileURL
        fileLink.setAttribute('download', payload.filename + '.xlsx')
        document.body.appendChild(fileLink)
        fileLink.click()
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getVat({ commit }) {
    commit('LOADING')
    return MainService.getVat()
      .then(function(response) {
        commit('UPDATE_VAT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getCbm({ commit }) {
    commit('LOADING')
    return MainService.getCbm()
      .then(function(response) {
        commit('UPDATE_CBM', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getCurrency({ commit }) {
    commit('LOADING')
    return MainService.getCurrency()
      .then(function(response) {
        commit('UPDATE_CURRENCY', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  getForeignExchangeRate({ commit }, payload) {
    commit('LOADING')
    return MainService.getForeignExchangeRate(payload)
      .then(function(response) {
        commit('UPDATE_FOREIGN_EXCHANGE_RATE', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postCalculate({ state, commit }, payload) {
    commit('LOADING')
    return MainService.postCalculate(payload)
      .then(function(response) {
        if (response.data.status == false || response.data.status == 'false') {
          state.dataResult = response.data.message
        } else {
          state.dataResult = response.data.dataSet
          state.dataSearch = {
            ...state.dataSearch,
            fromDate: moment(payload.FromDate).format('DD MMM YY'),
            toDate: moment(payload.ToDate).format('DD MMM YY'),
            productTypeQty: payload.ProductTypeQty,
            weight: payload.Weight,
            containerNo: payload.ContainerCount
          }
          // set Transport
          if (payload.TransportId > 0) {
            state.dataSearch.transport = state.transport.filter(t => {
              return t.transportId == payload.TransportId
            })[0].transportDesc
          } else {
            state.dataSearch.transport = 'ไม่ระบุ'
          }

          //set TransportType
          if (payload.TransportTypeId > 0) {
            state.dataSearch.transportType = state.transportType.filter(tt => {
              return tt.transportTypeId == payload.TransportTypeId
            })[0].transportTypeDesc
          } else {
            state.dataSearch.transportType = 'ไม่ระบุ'
          }

          //set From Country
          if (payload.FromCountryId > 0) {
            state.dataSearch.fromCountry = state.fromCountry.filter(fc => {
              return fc.countryId == payload.FromCountryId
            })[0].countryDesc
          } else {
            state.dataSearch.fromCountry = 'ไม่ระบุ'
          }

          //set From Country Location
          if (payload.FromCountryLocationId > 0) {
            state.dataSearch.fromCountryLocation =
              state.fromCountryLocation.filter(fcl => {
                return fcl.countryLocationId == payload.FromCountryLocationId
              })[0].locationCity +
              ' - ' +
              state.fromCountryLocation.filter(fcl => {
                return fcl.countryLocationId == payload.FromCountryLocationId
              })[0].locationName
          } else {
            state.dataSearch.fromCountryLocation = 'ไม่ระบุ'
          }

          //set To Country
          if (payload.ToCountryId > 0) {
            state.dataSearch.toCountry = state.toCountry.filter(tc => {
              return tc.countryId == payload.ToCountryId
            })[0].countryDesc
          } else {
            state.dataSearch.toCountry = 'ไม่ระบุ'
          }

          //set To Country Location
          if (payload.ToCountryLocationId > 0) {
            state.dataSearch.toCountryLocation =
              state.toCountryLocation.filter(tcl => {
                return tcl.countryLocationId == payload.ToCountryLocationId
              })[0].locationCity +
              ' - ' +
              state.toCountryLocation.filter(tcl => {
                return tcl.countryLocationId == payload.ToCountryLocationId
              })[0].locationName
          } else {
            state.dataSearch.toCountry = 'ไม่ระบุ'
          }

          //set Shipment
          if (payload.ShipmentId > 0) {
            state.dataSearch.shipment = state.shipment.filter(s => {
              return s.shipmentId == payload.ShipmentId
            })[0].shipmentDesc
          } else {
            state.dataSearch.shipment = 'ไม่ระบุ'
          }

          //set Container
          if (payload.ContainerId > 0) {
            state.dataSearch.container = state.container.filter(c => {
              return c.containerId == payload.ContainerId
            })[0].containerDesc
          } else {
            state.dataSearch.container = 'ไม่ระบุ'
          }
        }
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postResultDetail({ commit }, payload) {
    commit('LOADING')
    return MainService.postResultDetail(payload)
      .then(function(response) {
        state.dataResultDetail = response.data.dataSet
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postInvoice({ state, commit }, payload) {
    commit('LOADING')
    return MainService.postInvoice(payload)
      .then(function(response) {
        if (response.data.status) {
          state.dataResultHeader.invoice = response.data.dataSet.invoice
          state.dataResultHeader.note = response.data.dataSet.note
        }
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postMember({ commit }, payload) {
    commit('LOADING')
    return MainService.postMember(payload)
      .then(function(response) {
        console.log(response)
        if (!response.data.status) {
          commit('ERROR_MEMBER', response.data.message)
        } else {
          commit('ERROR_MEMBER', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_MEMBER', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postAgent({ commit }, payload) {
    commit('LOADING')
    return MainService.postAgent(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_AGENT', response.data.message)
        } else {
          commit('ERROR_AGENT', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_AGENT', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postVat({ commit }, payload) {
    commit('LOADING')
    return MainService.postVat(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_VAT', response.data.message)
        } else {
          commit('ERROR_VAT', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_VAT', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postForeignExchangeRate({ commit }, payload) {
    commit('LOADING')
    return MainService.postForeignExchangeRate(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_FOREIGN_EXCHANGE_RATE', response.data.message)
        } else {
          commit('ERROR_FOREIGN_EXCHANGE_RATE', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_FOREIGN_EXCHANGE_RATE', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postExpenseName({ commit }, payload) {
    commit('LOADING')
    return MainService.postExpenseName(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_EXPENSE_NAME', response.data.message)
        } else {
          commit('ERROR_EXPENSE_NAME', '')
          commit('UPDATE_EXPENSE_NAME', response.data.dataSet)
        }
      })
      .catch(function(error) {
        commit('ERROR_EXPENSE_NAME', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postRequest({ commit }, payload) {
    commit('LOADING')
    console.log(payload)
    return MainService.postRequest(payload)
      .then(function(response) {
        console.log(response)
        if (!response.data.status) {
          commit('ERROR_REQUEST', response.data.message)
        } else {
          commit('UPDATE_REQUEST', response.data.dataSet)
        }
      })
      .catch(function(error) {
        commit('ERROR_REQUEST', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postImportUpload({ commit }, payload) {
    commit('LOADING')
    return MainService.postImportUpload(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_IMPORT_UPLOAD', response.data.message)
        } else {
          commit('ERROR_IMPORT_UPLOAD', '')
          commit('UPDATE_IMPORT_UPLOAD', response.data.dataSet)
        }
      })
      .catch(function(error) {
        commit('ERROR_IMPORT_UPLOAD', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postImportReject({ commit }, payload) {
    commit('LOADING')
    return MainService.postImportReject(payload)
      .then(function(response) {
        commit('UPDATE_IMPORT', response.data.dataSet)
      })
      .catch(function(error) {
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  postSearchHistory({ commit }, payload) {
    commit('LOADING')
    console.log(payload)
    return MainService.postSearchHistory(payload)
      .then(function(response) {
        console.log(response)
        // if (!response.data.status) {
        //   commit('ERROR_REQUEST', response.data.message)
        // } else {
        //   commit('UPDATE_REQUEST', response.data.dataSet)
        // }

        state.histories = response.data.dataSet
      })
      .catch(function(error) {
        commit('ERROR_REQUEST', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },

  putMember({ commit }, payload) {
    commit('LOADING')
    return MainService.putMember(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_MEMBER', response.data.message)
        } else {
          commit('ERROR_MEMBER', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_MEMBER', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putAgent({ commit }, payload) {
    commit('LOADING')
    return MainService.putAgent(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_AGENT', response.data.message)
        } else {
          commit('ERROR_AGENT', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_AGENT', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putCbm({ commit }, payload) {
    commit('LOADING')
    return MainService.putCbm(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_CBM', response.data.message)
        } else {
          commit('ERROR_CBM', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_CBM', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putVat({ commit }, payload) {
    commit('LOADING')
    return MainService.putVat(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_VAT', response.data.message)
        } else {
          commit('ERROR_VAT', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_VAT', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putForeignExchangeRate({ commit }, payload) {
    commit('LOADING')
    return MainService.putForeignExchangeRate(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_FOREIGN_EXCHANGE_RATE', response.data.message)
        } else {
          commit('ERROR_FOREIGN_EXCHANGE_RATE', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_FOREIGN_EXCHANGE_RATE', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putExpenseDetail({ commit }, payload) {
    commit('LOADING')
    return MainService.putExpenseDetail(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_EXPENSE_DETAIL', response.data.message)
        } else {
          commit('ERROR_EXPENSE_DETAIL', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_EXPENSE_DETAIL', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putExpenseName({ commit }, payload) {
    commit('LOADING')
    return MainService.putExpenseName(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_EXPENSE_NAME', response.data.message)
        } else {
          commit('ERROR_EXPENSE_NAME', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_EXPENSE_NAME', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putExpenseAgent({ commit }, payload) {
    commit('LOADING')
    return MainService.putExpenseAgent(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_EXPENSE_AGENT', response.data.message)
        } else {
          commit('ERROR_EXPENSE_AGENT', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_EXPENSE_AGENT', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putImportDetail({ commit }, payload) {
    commit('LOADING')
    return MainService.putImportDetail(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_IMPORT_DETAIL', response.data.message)
        } else {
          commit('ERROR_IMPORT_DETAIL', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_IMPORT_DETAIL', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  },
  putImportPrice({ commit }, payload) {
    commit('LOADING')
    return MainService.putImportPrice(payload)
      .then(function(response) {
        if (!response.data.status) {
          commit('ERROR_IMPORT_PRICE', response.data.message)
        } else {
          commit('ERROR_IMPORT_PRICE', '')
        }
      })
      .catch(function(error) {
        commit('ERROR_IMPORT_PRICE', 'Server ERROR.')
        console.log(error)
      })
      .then(function() {
        commit('LOAD_DONE')
      })
  }
}
export default new Vuex.Store({
  state,
  mutations,
  actions
})
