import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routerOptions = [
  {
    path: '/',
    name: 'home',
    component: 'Home',
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'login',
    component: 'Login'
  },
  {
    path: '/calculator',
    name: 'calculator',
    component: 'Calculator',
    meta: { requiresAuth: true }
  },
  {
    path: '/calculateDetail/:type/:id',
    name: 'calculateDetail',
    component: 'CalculateDetail',
    meta: { requiresAuth: true }
  },
  {
    path: '/history',
    name: 'history',
    component: 'History',
    meta: { requiresAuth: true }
  },
  {
    path: '/historyDetail/:type/:id',
    name: 'historyDetail',
    component: 'HistoryDetail',
    meta: { requiresAuth: true }
  },
  {
    path: '/member',
    name: 'member',
    component: 'Member',
    meta: { requiresAuth: true }
  },
  {
    path: '/memberManaging',
    name: 'memberManaging',
    component: 'MemberManaging',
    meta: { requiresAuth: true }
  },
  {
    path: '/memberManaging/:id',
    name: 'memberManaging',
    component: 'MemberManaging',
    meta: { requiresAuth: true }
  },
  {
    path: '/agent',
    name: 'agent',
    component: 'Agent',
    meta: { requiresAuth: true }
  },
  {
    path: '/agentManaging',
    name: 'agentManaging',
    component: 'AgentManaging',
    meta: { requiresAuth: true }
  },
  {
    path: '/agentManaging/:id',
    name: 'agentManaging',
    component: 'AgentManaging',
    meta: { requiresAuth: true }
  },
  {
    path: '/expense',
    name: 'expense',
    component: 'Expense',
    meta: { requiresAuth: true }
  },
  {
    path: '/expenseAdd',
    name: 'expenseAdd',
    component: 'ExpenseAdd',
    meta: { requiresAuth: true }
  },
  {
    path: '/expenseView/:id',
    name: 'expenseView',
    component: 'ExpenseView',
    meta: { requiresAuth: true }
  },
  {
    path: '/import',
    name: 'import',
    component: 'Import',
    meta: { requiresAuth: true }
  },
  {
    path: '/importDetail/:transportId/:transportTypeId/:status/:id',
    name: 'importDetail',
    component: 'ImportDetail',
    meta: { requiresAuth: true }
  },
  {
    path: '/importPrice/:detailId/:transportTypeId/:transportId/:status/:id',
    name: 'importPrice',
    component: 'ImportPrice',
    meta: { requiresAuth: true }
  },
  {
    path: '/request',
    name: 'request',
    component: 'Request',
    meta: { requiresAuth: true }
  },
  {
    path: '/vat',
    name: 'vat',
    component: 'Vat',
    meta: { requiresAuth: true }
  },
  {
    path: '/cbm',
    name: 'cbm',
    component: 'Cbm',
    meta: { requiresAuth: true }
  },
  {
    path: '/exchange',
    name: 'exchange',
    component: 'Exchange',
    meta: { requiresAuth: true }
  }
]

const routes = routerOptions.map(route => {
  return {
    path: route.path,
    name: route.name,
    component: () => import(`./views/${route.component}.vue`),
    meta: route.meta
  }
})

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    ...routes,
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  var minute = 20

  var now = new Date().getTime()
  var setupTime = localStorage.getItem('setupTime')
  if (setupTime == null) {
    localStorage.setItem('setupTime', now)
  } else {
    if (now - setupTime > minute * 60 * 1000) {
      localStorage.clear()
      next('/login')
    } else {
      localStorage.setItem('setupTime', now)
    }
  }
  let loggedIn = false
  // console.log(localStorage.getItem('loginToken'))

  if (localStorage.getItem('loginToken') != null) {
    loggedIn = true
  }

  if (requiresAuth && !loggedIn) {
    next('/login')
  } else {
    next()
  }
})

export default router
