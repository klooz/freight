import axios from 'axios'

const apiClient = axios.create({
  // baseURL: `http://10.8.198.104/v1`,
  // baseURL: `http://10.8.241.104/v1`,
  baseURL: `http://apifreight.chiataigroup.com/v1`,
  // baseURL: `http://api-uat.chiataigroup.com/v1`,
  withCredentials: false, // This is the default
  timeout: 30000
})

export default {
  // GET
  getCommon(url) {
    return apiClient.get(url, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  accessToken(u, p) {
    return apiClient.get('/login/' + u + '/' + p)
  },
  checkTokenExpired() {
    return apiClient.get('/CheckToken', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getMenu() {
    return apiClient.get('/menu', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getDashboard() {
    return apiClient.get('/Dashboard')
  },
  getTransport() {
    return apiClient.get('/Transport', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getTransportType() {
    return apiClient.get('/TransportType', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getCountry(transportId, transportTypeId, from_to) {
    return apiClient.get(
      '/Country/' + transportId + '/' + transportTypeId + '/' + from_to,
      {
        headers: { Authen: localStorage.getItem('loginToken') }
      }
    )
  },
  getCountryLocation(transportTypeId, countryId) {
    return apiClient.get(
      '/countrylocation/' + transportTypeId + '/' + countryId,
      {
        headers: { Authen: localStorage.getItem('loginToken') }
      }
    )
  },
  getShipment() {
    return apiClient.get('/Shipment', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getContainers() {
    return apiClient.get('/Containers', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getResultHeader(id) {
    return apiClient.get('/ResultHeader/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getResultDetail(id) {
    return apiClient.get('/ResultDetail/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getHistory(search) {
    return apiClient.get('/History/' + search, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getHistoryDataSearch() {
    return apiClient.get('/DataSearch', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getResultHeaderHistory(id) {
    return apiClient.get('/ResultHeaderHistory/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getInvoice(id) {
    return apiClient.get('/Invoice/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getMember(id) {
    let searchId = id ? '/' + id : ''
    return apiClient.get('/Member' + searchId, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getDepartment() {
    return apiClient.get('/Department ', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getAllDepartment() {
    return apiClient.get('/AllDepartment ', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getUserType() {
    return apiClient.get('/UserType', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getAgent(arr) {
    let p = ''
    if (arr) {
      arr.forEach(item => {
        p = p + '/' + item
      })
    }
    return apiClient.get('/Agent' + p, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getExpense(id) {
    let searchId = id ? '/' + id : ''
    return apiClient.get('/Expense' + searchId, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getExpenseName() {
    return apiClient.get('/ExpenseName', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getExpenseDetail(id) {
    return apiClient.get('/ExpenseDetail/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getExpenseDateDetail(id) {
    return apiClient.get('/ExpenseDateDetail/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getRequest(id) {
    let searchId = id ? '/' + id : ''
    return apiClient.get('/Request' + searchId, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getRequestApprove(body) {
    return apiClient.get('/RequestApprove/' + body, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getRequestCancel(body) {
    return apiClient.get('/RequestCancel/' + body, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getStatus() {
    return apiClient.get('/Status', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getImportList(transportId, transportTypeId, statusId) {
    return apiClient.get(
      '/ImportList/' + transportId + '/' + transportTypeId + '/' + statusId,
      {
        headers: { Authen: localStorage.getItem('loginToken') }
      }
    )
  },
  getImportDetail(id, transportTypeId) {
    return apiClient.get('/ImportDetail/' + id + '/' + transportTypeId, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getImportPrice(detailId, transportTypeId) {
    return apiClient.get('/ImportPrice/' + detailId + '/' + transportTypeId, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getImportDownload(transportTypeId, tableId) {
    return apiClient.get('/Download/' + transportTypeId + '/' + tableId, {
      responseType: 'blob',
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getImportApprove(transportTypeId, id) {
    return apiClient.get('/ApproveFreight/' + transportTypeId + '/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getImportExpire(transportTypeId, id) {
    return apiClient.get('/ExpireFreight/' + transportTypeId + '/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getExpenseDownload(tableId) {
    return apiClient.get('/DownloadExpense/' + tableId, {
      responseType: 'blob',
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getVat() {
    return apiClient.get('/Vat', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getCbm() {
    return apiClient.get('/Cbm', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getCurrency() {
    return apiClient.get('/Currency', {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  getForeignExchangeRate(id) {
    return apiClient.get('/ExchangeRate/' + id, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },

  // POST
  postCommon(url, body) {
    return apiClient.post(url, body, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  postCalculate(body) {
    return apiClient.post('/Calculate', body, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  postResultDetail(body) {
    return apiClient.post('/ResultDetail', body, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  postInvoice(body) {
    return apiClient.post('/Invoice', body, {
      headers: { Authen: localStorage.getItem('loginToken') }
    })
  },
  postMember(body) {
    return apiClient.post('/Member', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  postAgent(body) {
    return apiClient.post('/Agent', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  postVat(body) {
    return apiClient.post('/Vat', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  postForeignExchangeRate(body) {
    return apiClient.post('/ExchangeRate', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  postExpenseName(body) {
    return apiClient.post('/ExpenseName', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  postRequest(body) {
    return apiClient.post('/Request', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  postImportUpload(body) {
    return apiClient.post('/Upload', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'multipart/form-data'
      }
    })
  },
  postImportReject(body) {
    return apiClient.post('/RejectFreight', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  postSearchHistory(body) {
    return apiClient.post('/History', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },

  //PUT
  putCommon(url, body) {
    return apiClient.post(url, body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putMember(body) {
    return apiClient.put('/Member', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putAgent(body) {
    return apiClient.put('/Agent', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putCbm(body) {
    return apiClient.put('/Cbm', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putVat(body) {
    return apiClient.put('/Vat', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putForeignExchangeRate(body) {
    return apiClient.put('/ExchangeRate', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putExpenseName(body) {
    return apiClient.put('/ExpenseName', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putExpenseDetail(body) {
    return apiClient.put('/ExpenseDetail', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putExpenseAgent(body) {
    return apiClient.put('/ExpenseAgent', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putImportDetail(body) {
    return apiClient.put('/ImportDetail', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  },
  putImportPrice(body) {
    return apiClient.put('/ImportPrice', body, {
      headers: {
        Authen: localStorage.getItem('loginToken'),
        'Content-Type': 'application/json'
      }
    })
  }
}
