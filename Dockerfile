# FROM httpd:2.4 AS base
# WORKDIR /usr/local/apache2/htdocs/
# EXPOSE 80
# EXPOSE 443

# FROM base AS final
# WORKDIR /usr/local/apache2/htdocs/
# COPY . .

# build stage
FROM node:9.11.1-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/nginx/default.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
